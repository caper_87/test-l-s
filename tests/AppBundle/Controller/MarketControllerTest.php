<?php

namespace Tests\AppBundle\Controller;

use Tests\AppBundle\FunctionalTestCase;

class MarketControllerTest extends FunctionalTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Welcome to APP', $crawler->filter('h1')->text());

        $this->assertContains('Dow Jones Industrial Average', $crawler->filter('.tbl-row .col-lg-3')->text());
    }

    public function testGetIndex()
    {
        $client = static::createClient();

        $client->request('GET', '/market/idx/1/update');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertNotEmpty($crawler->filter('#idx_open')->text());
        $this->assertNotEmpty($crawler->filter('#idx_hight')->text());
        $this->assertNotEmpty($crawler->filter('#idx_low')->text());
    }

    public function testGetXml()
    {
        $client = static::createClient();

        $client->request('GET', '/market/1/xml');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'content-type',
                'text/xml; charset=UTF-8'
            ),
            'the "Content-Type" header is "text/xml"'
        );
    }

    public function testGetDailyMarketsReportPage()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/market/report');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertNotEmpty($crawler->filter('#report-form h2')->text());
    }

    public function testSendDailyMarketsReportAction()
    {
        $client = static::createClient();

        $crawler = $client->request('POST', '/market/report');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Thank you, the report successfully send on your email', $crawler->filter('#report_success')->text());
    }

    public function testGetChart()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/market/chart');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filter('#market-chart'));
    }
}
