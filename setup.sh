#!/usr/bin/env bash
mkdir app/db

bin/console doctrine:database:create
bin/console doctrine:migrations:migrate --no-interaction

sudo chmod -R 777 var/