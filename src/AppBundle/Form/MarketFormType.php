<?php

namespace AppBundle\Form;

use AppBundle\Entity\Market;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MarketFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'invalid_message' => 'Invalid data for title'
            ])
            ->add('code', TextType::class, [
                'invalid_message' => 'Invalid data for code'
            ])
            ->add('price', NumberType::class, [
                'invalid_message' => 'Invalid data for price'
            ])
            ->add('percent', NumberType::class, [
                'invalid_message' => 'Invalid data for percent'
            ])
            ->add('percent_direction', IntegerType::class, [
                'invalid_message' => 'Invalid data for percent_direction',
            ])
            ->add('idx_open', TextType::class, [
                'invalid_message' => 'Invalid data for idx_open',
                'required' => false,
            ])
            ->add('idx_hight', TextType::class, [
                'invalid_message' => 'Invalid data for idx_hight',
                'required' => false,
            ])
            ->add('idx_low', NumberType::class, [
                'invalid_message' => 'Invalid data for idx_low',
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Market::class,
        ]);
    }
}