<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Market
 *
 * @ORM\Table(name="market")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MarketRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Market
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, unique=true)
     */
    private $code;

    /**
     * @var float
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/[+-]?([0-9]*[.])?[0-9]+/")
     * @ORM\Column(name="price", type="float")
     */
    private $price;

    /**
     * @var float
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/[+-]?([0-9]*[.])?[0-9]+/")
     * @ORM\Column(name="percent", type="float")
     */
    private $percent;

    /**
     * @var int
     * @Assert\Regex(pattern="/(-1|0|1)/")
     * @ORM\Column(name="percent_direction", type="integer")
     */
    private $percentDirection;

    /**
     * @var float
     * @Assert\Regex(pattern="/[+-]?([0-9]*[.])?[0-9]+/")
     * @ORM\Column(name="idx_open", type="float", nullable=true)
     */
    private $idxOpen;

    /**
     * @var float
     * @Assert\Regex(pattern="/[+-]?([0-9]*[.])?[0-9]+/")
     * @ORM\Column(name="idx_hight", type="float", nullable=true)
     */
    private $idxHight;

    /**
     * @var float
     * @Assert\Regex(pattern="/[+-]?([0-9]*[.])?[0-9]+/")
     * @ORM\Column(name="idx_low", type="float", nullable=true)
     */
    private $idxLow;

    /**
     * @ORM\Column(name="created", type="integer")
     */
    private $created;

    /**
     * @ORM\Column(name="updated", type="integer")
     */
    private $updated;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Market
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Market
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Market
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set percent
     *
     * @param float $percent
     *
     * @return Market
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * Get percent
     *
     * @return float
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * Set percentDirection
     *
     * @param integer $percentDirection
     *
     * @return Market
     */
    public function setPercentDirection($percentDirection)
    {
        $this->percentDirection = $percentDirection;

        return $this;
    }

    /**
     * Get percentDirection
     *
     * @return int
     */
    public function getPercentDirection()
    {
        return $this->percentDirection;
    }

    /**
     * Set idxOpen
     *
     * @param float $idxOpen
     *
     * @return Market
     */
    public function setIdxOpen($idxOpen)
    {
        $this->idxOpen = $idxOpen;

        return $this;
    }

    /**
     * Get idxOpen
     *
     * @return float
     */
    public function getIdxOpen()
    {
        return $this->idxOpen;
    }

    /**
     * Set idxHight
     *
     * @param float $idxHight
     *
     * @return Market
     */
    public function setIdxHight($idxHight)
    {
        $this->idxHight = $idxHight;

        return $this;
    }

    /**
     * Get idxHight
     *
     * @return float
     */
    public function getIdxHight()
    {
        return $this->idxHight;
    }

    /**
     * Set idxLow
     *
     * @param float $idxLow
     *
     * @return Market
     */
    public function setIdxLow($idxLow)
    {
        $this->idxLow = $idxLow;

        return $this;
    }

    /**
     * Get idxLow
     *
     * @return float
     */
    public function getIdxLow()
    {
        return $this->idxLow;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return (new \DateTime())->setTimestamp($this->created);
    }

    /**
     * Set created
     * @ORM\PrePersist
     * @return Market
     */
    public function setCreated()
    {
        $this->created = (new \DateTime())->getTimestamp();
        $this->updated = (new \DateTime())->getTimestamp();
        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return (new \DateTime())->setTimestamp($this->updated);
    }

    /**
     * Set updated
     * @ORM\PreUpdate
     * @return Market
     */
    public function setUpdated()
    {
        $this->updated = (new \DateTime())->getTimestamp();
        return $this;
    }
}

