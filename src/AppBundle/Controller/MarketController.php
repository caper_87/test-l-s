<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Market;
use AppBundle\Form\MarketReportFormType;
use AppBundle\Managers\MarketManager;
use AppBundle\Repository\MarketRepository;
use AppBundle\Utils\HttpClient;
use AppBundle\Utils\Mailer;
use AppBundle\Utils\Parser;
use AppBundle\Utils\XMLBuilder;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MarketController extends Controller
{
    /**
     * @var MarketRepository
     */
    public $repository;

    public function __construct(ObjectManager $em)
    {
        $this->repository = $em->getRepository(Market::class);
    }

    /**
     * @Route("/", methods={"GET"}, name="index")
     * @param HttpClient $client
     * @param Parser $parser
     * @param MarketManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(HttpClient $client, Parser $parser, MarketManager $manager)
    {
        $content = $client->getMarketsHtml();
        $marketsData = $parser->parseMarkets($content);
        $markets = $manager->multiCreate($marketsData);
        return $this->render('default/index.html.twig', [
            'markets' => $markets,
        ]);
    }

    /**
     * @Route("/market/idx/{id}/update", methods={"GET"}, name="idx_update")
     * @param Market $market
     * @param HttpClient $client
     * @param Parser $parser
     * @param MarketManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getIndexAction(Market $market, HttpClient $client, Parser $parser, MarketManager $manager)
    {
        $content = $client->getIndexesHtml($market->getCode());
        $indexData = $parser->parseIndexes($content);
        $manager->update($market, $indexData);
        $markets = $this->repository->findAll();
        return $this->redirectToRoute('index', [
            'markets' => $markets,
        ]);
    }

    /**
     * @Route("/market/{id}/xml", defaults={"_format"="xml"}, methods={"GET"}, name="get_xml")
     * @param Market $market
     * @param XMLBuilder $builder
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getXmlAction(Market $market, XMLBuilder $builder)
    {
        $xml = $builder->build($market);
        return new Response($xml);
    }

    /**
     * @Route("/market/report", methods={"GET"}, name="report")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getDailyMarketsReportPageAction()
    {
        $form = $this->createForm(MarketReportFormType::class);
        return $this->render('default/report.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/market/report", methods={"POST"}, name="report_send")
     * @param Request $request
     * @param XMLBuilder $builder
     * @param Mailer $mailer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function sendDailyMarketsReportAction(Request $request, XMLBuilder $builder, Mailer $mailer)
    {
        $markets = $this->repository->findDailyHighest();
        $xml = $builder->buildAll($markets);
        $mailer->sendMessage('market_report', ['markets' => $markets], $xml, $request->get('market_report_form')['email']);

        return $this->render('default/report.html.twig', [
            'send' => true,
        ]);
    }

    /**
     * @Route("/market/chart", methods={"GET"}, name="get_chart")
     * @return Response
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getChartAction()
    {
        $dataForCharts = $this->repository->findDataForCharts();
        return $this->render('default/chart.html.twig', [
            'data' => json_encode($dataForCharts, true),
        ]);
    }
}
