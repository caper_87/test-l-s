<?php

namespace AppBundle\Managers;

use AppBundle\Entity\Market;
use AppBundle\Form\FormHandler;
use AppBundle\Form\MarketFormType;
use AppBundle\Form\MarketIndexFormType;
use Doctrine\Common\Persistence\ObjectManager;

class MarketManager
{
    private $em;
    private $formHandler;

    public function __construct(ObjectManager $em, FormHandler $formHandler)
    {
        $this->em = $em;
        $this->formHandler = $formHandler;
    }

    /**
     * @param array $marketPool
     * @return array
     */
    public function multiCreate(array $marketPool)
    {
        $markets = [];
        foreach ($marketPool as $market) {
            $markets[] = $this->createOrUpdate($market);
        }
        return $markets;
    }

    /**
     * @param array $marketData
     * @return Market
     */
    public function createOrUpdate(array $marketData)
    {
        $repo = $this->getRepository(Market::class);
        if ($market = $repo->findOneBy(['code' => $marketData['code']])) {
            return $this->update($market, $marketData);
        }
        return $this->create($marketData);
    }

    /**
     * @param array $marketData
     * @return Market
     */
    public function create(array $marketData)
    {
        /** @var Market $market */
        $market = $this->formHandler->handle(MarketFormType::class, new Market(), $marketData);
        $this->save($market);
        return $market;
    }

    public function update(Market $market, $marketData)
    {
        /** @var Market $market */
        $market = $this->formHandler->handle(MarketFormType::class, $market, $marketData, [], false);
        $this->save($market);
        return $market;
    }

    /**
     * @param Market $market
     */
    public function save(Market $market)
    {
        $this->em->persist($market);
        $this->em->flush();
    }

    /**
     * @param $class
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getRepository($class)
    {
        return $this->em->getRepository($class);
    }
}