<?php

namespace AppBundle\Utils;

use AppBundle\Logger\ParserLogger;
use Symfony\Component\DomCrawler\Crawler;

class Parser
{
    const PERCENT_UP = '+';

    const PERCENT_DOWN = '−';

    /**
     * @var Crawler
     */
    private $crawler;

    private $logger;

    /**
     * Parser constructor.
     * @param Crawler $crawler
     * @param ParserLogger $logger
     */
    public function __construct(Crawler $crawler, ParserLogger $logger)
    {
        $this->crawler = $crawler;
        $this->logger = $logger;
    }

    /**
     * @param $content
     * @return mixed
     */
    public function parseMarkets($content)
    {
        try {
            $this->crawler->add($content);
            $worldMarketsWrap = $this->crawler->filter('.rit-block')->children()->eq(1);

            $nodeValues = $worldMarketsWrap->filter('.QEsAKd')->each(function (Crawler $node) {
                return [
                    'title'   => $node->filter('.tdHJPc')->text(),
                    'price'   => $node->filter('.E4K4Td')->text(),
                    'code'    => $node->filter('.UKEYAb')->eq(0)->text(),
                    'percent' => $node->filter('.GszhFb')->text(),
                ];
            });

            $markets = $this->postParseMarketsUpdate($nodeValues);

            $this->logger->info("Markets successfully parsed");

            return $markets;
        } catch (\Exception $exception) {
            $this->logger->error("Markets parsing error!");
        }
    }

    /**
     * @param $content
     * @return array
     */
    public function parseIndexes($content)
    {
        try {
            $this->crawler->add($content);
            $nodes = $this->crawler->filter('.aviV4d')->filter('.QhLvnd')->filter('.iyjjgb');
            $indexes = [
                'idx_open'   => $nodes->eq(0)->text(),
                'idx_hight'   => $nodes->eq(1)->text(),
                'idx_low'    => $nodes->eq(2)->text(),
            ];

            $indexes = $this->postParseIndexesUpdate($indexes);

            $this->logger->info("Markets indexes successfully parsed");

            return $indexes;
        } catch (\Exception $exception) {
            $this->logger->error("Markets indexes parsing error!");
        }
    }

    /**
     * @param $nodeValues
     * @return mixed
     */
    public function postParseMarketsUpdate($nodeValues)
    {
        foreach ($nodeValues as $key => $nodeValue) {
            $title   = $nodeValue['title'];
            $price   = $this->parsePrice($nodeValue['price']);
            $code    = $this->parseCode($nodeValue['code']);
            $percent = $this->parsePercent($nodeValue['percent']);
            $percent_direction = $this->parsePercentDirection($nodeValue['percent']);

            $nodeValues[$key] = compact('title','price' , 'code', 'percent', 'percent_direction');
        }
        return $nodeValues;
    }

    /**
     * @param $indexes
     * @return array
     */
    public function postParseIndexesUpdate($indexes)
    {
        $idx_open   = $this->parsePrice($indexes['idx_open']);
        $idx_hight  = $this->parsePrice($indexes['idx_hight']);
        $idx_low    = $this->parsePrice($indexes['idx_low']);
        return compact('idx_open','idx_hight' , 'idx_low');
    }

    /**
     * @param $str
     * @return float
     */
    public function parsePercent($str)
    {
        preg_match('/\((.*)%/', $str, $matches);
        return (float) str_replace(',', '.', $matches[1]);
    }

    /**
     * @param $str
     * @return int
     */
    public function parsePercentDirection($str)
    {
        $direction = mb_substr($str, 0, 1);
        if ($direction == self::PERCENT_UP) {
            $percent_direction = 1;
        } elseif ($direction == self::PERCENT_DOWN) {
            $percent_direction = -1;
        } else {
            $percent_direction = 0;
        }
        return $percent_direction;
    }

    /**
     * @param $str
     * @return float
     */
    public function parsePrice($str)
    {
        $price = str_replace(" ", "", $str);
        return (float) str_replace(',', '.', $price);
    }

    /**
     * @param $str
     * @return mixed
     */
    public function parseCode($str)
    {
        $code = preg_split('/-/', $str)[0];
        return str_replace(" ", "", $code);
    }
}