<?php

namespace AppBundle\Utils;

use AppBundle\Entity\Market;
use AppBundle\Logger\XmlBuilderLogger;

class XMLBuilder
{
    private $logger;

    public function __construct(XmlBuilderLogger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param Market $market
     * @return string
     */
    public function build(Market $market)
    {
        try {
            $xml = $this->buildMarket(new \SimpleXMLElement("<market/>"), $market);

            $this->logger->info("Market xml successfully generated");

            return $xml->asXML();
        } catch (\Exception $exception) {
            $this->logger->error("Error build market xml!");
        }
    }

    /**
     * @param array $markets
     * @return string
     */
    public function buildAll(array $markets)
    {
        try {
            $rootXml = new \SimpleXMLElement("<markets/>");
            foreach ($markets as $market) {
                $this->buildMarket($rootXml->addChild('market'), $market);
            }

            $this->logger->info("Markets xml successfully build");

            return $rootXml->asXML();
        } catch (\Exception $exception) {
            $this->logger->error("Error build markets xml");
        }
    }

    /**
     * @param \SimpleXMLElement $xml
     * @param Market $market
     * @return \SimpleXMLElement
     */
    public function buildMarket(\SimpleXMLElement $xml, Market $market)
    {
        $xml->addChild('title', $market->getTitle());
        $xml->addChild('code', $market->getCode());
        $xml->addChild('price', $market->getPrice());
        $xml->addChild('percent', $market->getPercent());
        $xml->addChild('percent_direction', $market->getPercentDirection());
        $xml->addChild('idx_open', $market->getIdxOpen());
        $xml->addChild('idx_hight', $market->getIdxHight());
        $xml->addChild('idx_low', $market->getIdxLow());
        return $xml;
    }
}