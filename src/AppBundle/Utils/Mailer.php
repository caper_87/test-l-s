<?php

namespace AppBundle\Utils;


use AppBundle\Logger\MailLogger;
use Monolog\Logger;

class Mailer
{
    private $mailer;
    private $twig;
    private $logger;
    public $fromEmail = 'send@example.com';

    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $twig, MailLogger $logger)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->logger = $logger;
    }

    public function sendMessage($templateName, $data, $attachment, $toEmail)
    {
        try {
            $template = $this->twig->render("Emails/{$templateName}.html.twig", $data);
            $message = (new \Swift_Message('Hello Email'))
                ->setFrom($this->fromEmail)
                ->setBody($template, 'text/html')
                ->attach(\Swift_Attachment::newInstance($attachment, 'markets.xml', 'application/xml'));

            $this->mailer->send($message);

            $this->logger->info("Markets Report successfully send to {$toEmail}");
        } catch (\Exception $exception) {
            $this->logger->error("Markets Report error!");
        }
    }
}