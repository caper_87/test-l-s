<?php

namespace AppBundle\Utils;


use AppBundle\Logger\HttpClientLogger;

class HttpClient
{
    private $logger;

    public function __construct(HttpClientLogger $logger)
    {
        $this->logger = $logger;
    }

    public function getMarketsHtml()
    {
        $url = "https://www.google.com/search?stick=H4sIAAAAAAAAAOPQeMSozC3w8sc9YSmpSWtOXmMU4RJyy8xLzEtO9UnMS8nMSw9ITE_lAQCCiJIYKAAAAA&q=finance&tbm=fin#scso=uid_MzMUW8aQG6Pv6QTWoIGgCw_0:0";
        return $this->sendRequest($url);
    }

    public function getIndexesHtml($marketCode)
    {
        $url = "https://www.google.com/search?q={$marketCode}";
        return $this->sendRequest($url);
    }

    public function sendRequest($url)
    {
        try {
            $headers = [
                'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                'Accept-Encoding: gzip, deflate',
                'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
                'Connection: keep-alive',
                'User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0'
            ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $result = gzdecode(curl_exec($ch));

            curl_close($ch);

            $this->logger->info("Successfully request to {$url}");

            return $result;
        } catch (\Exception $exception) {
            $this->logger->error("Request error! Url: {$url}");
        }
    }
}