<?php

namespace AppBundle\Logger;


class ParserLogger
{
    public $logger;

    public function __construct($logger)
    {
        $this->logger = $logger;
    }

    public function info($message)
    {
        $this->logger->info($message);
    }

    public function error($message)
    {
        $this->logger->error($message);
    }
}